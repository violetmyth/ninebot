def list2string(inList):
    if not inList:
        return "[]"
        
    result = "["
    for x in inList:
        result = result + str(x) + " "
    result = result[0:len(result) - 1] + "]"
    return result