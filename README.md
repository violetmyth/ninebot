# Ninebot for Discord #
Copyright 2020 - 2022 by violetmyth

# Commands #
The commands below assume that `signalChar` for your server is set to `!`

## Dice
### !roll XdY(+/-)Z
Rolls Y-sided dice X times, and adds/subtracts Z.
You can have multiple XdY and/or Z terms. Separate each with + or - as needed

Example: `!roll 3d6+2-1d4`

### !rollhits XdY T
Rolls Y-sided dice X times, and counts the number of them with values equal to or greater than T 

Example: `!rollhits 5d6 4`

### !lr Ax Dy z
Rolls a [Lancer](https://massif.netlify.app/lancer)-style check, where the following is true:

- x is the number of accuracy dice
- y is the number of difficulty dice
- z is the static bonus/penalty granted from things like backgrounds or mech skills

You can omit Ax, Dy, or z if none of those apply to your roll.

Example `!lr A2 D1 2`

### !wod Xd10 Y (S|C)
Rolls a World of Darkness-style check, where the following is true:

- X is your dice pool
- Y is your target number. Defaults to 7 if not specified
- S, if present, specifies that the skill is a specialization
- C, if present, specifies that this is an Exalted vs WoD roll for a Caste charm

You can omit S or C if they don't apply

Example `!wod 7d10 7 SC`

## Other
### !combosummon

Generates a random combo summon from Granblue Fantasy