from random import randint
import re

class NoTargetError(Exception):
    pass

class BadExpressionError(Exception):
    pass

def rollSingleDie( sides ):
    return randint( 1, sides )

def trimText( expression ):
    trimmer = re.compile("(\-?[0-9]*d?[0-9]+[\+\-]?)+")
    return trimmer.match(expression).group(0)

def parseDiceExpression( expression ):
    splitters=re.compile("(\+)|(\-)")

    elements = splitters.split( trimText(expression) )

    while None in elements:
        elements.remove(None)
    if elements[0] == '':
        elements.remove('')

    diceGroups = []
    totalModifiers = 0
    groupSign = 1

    for i in range( 0, len(elements) ):

        if elements[i] == "+":
            groupSign = 1
        elif elements[i] == "-":
            groupSign = -1
        elif "d" not in elements[i]:
            totalModifiers += groupSign * int(elements[i])
        else:
            if re.match("^[1-9][0-9]*",elements[i]):
                numDice = elements[i].split("d")[0]
            elif re.match("^0",elements[i]):
                continue
            else:
                numDice = 1

            diceSides = elements[i].split("d")[1]
            newGroup = {
                'sign':groupSign,
                'numDice':int(numDice),
                'diceSides':int(diceSides)
            }
            diceGroups.append(newGroup)
    
    results = {
        'diceGroups':diceGroups,
        'totalModifiers':totalModifiers
    }
    return results

def rollDice( expression ):
    data = parseDiceExpression( expression )
    diceGroups = data["diceGroups"]
    totalModifiers = data["totalModifiers"]
    diceList = []

    for i in diceGroups:
        for j in range(0,i["numDice"]):
            diceList.append( i["sign"] * rollSingleDie( i["diceSides"] ) )        
    
    diceList.sort()

    results = {
        'expression':trimText(expression),
        'sum':sum(diceList)+totalModifiers,
        'dice':diceList
    }

    return results

def rollHits( expression ):
    if not re.search("\ [0-9]+.*$", expression):
        raise NoTargetError
    
    targetExpr = re.search("\ [0-9]+.*$", expression).group(0).replace(" ","")
    targetNumber = int(re.match("[0-9]+", targetExpr).group(0))

    data = rollDice( expression )

    numHits = 0
    for i in data["dice"]:
        if int(i) >= targetNumber:
            numHits += 1

    data["targetNumber"] = targetNumber
    data["numHits"] = numHits

    return data

def rollLancer( expression ):
    rollTerms = expression.split()

    accuracyExpr = "^[Aa]{1}([0-9]+)$"
    difficultyExpr = "^[Dd]{1}([0-9]+)$"
    statExpr = "^((\+|\-)?[1-9]+)$"
    statModifier = 0
    numBonusDice = 0
    bonusType = ""
    bonusDice = []

    baseRoll = rollSingleDie(20)
    
    for i in rollTerms:
        if re.match(accuracyExpr, i):
            numBonusDice += int( re.match(accuracyExpr, i).group(1) )
        elif re.match(difficultyExpr, i):
            numBonusDice -= int( re.match(difficultyExpr, i).group(1) )
        elif re.match(statExpr, i):
            statModifier += int( re.match(statExpr, i).group(1) )

    if numBonusDice != 0:
        for i in range(0, abs(numBonusDice)):
            bonusDice.append( rollSingleDie(6) )

        bonusDice.sort()
        
        if numBonusDice > 0:
            bonusType = 1
        elif numBonusDice < 0:
            bonusType = -1
        else:
            bonusType = 0

    if bonusType == 1:
        rollSum = baseRoll + max(bonusDice) + statModifier
    elif bonusType == -1:
        rollSum = baseRoll - max(bonusDice) + statModifier
    else:
        rollSum = baseRoll + statModifier
    
    results = {
        "expression":re.sub("^\ *\+","",expression),
        "baseRoll":baseRoll,
        "bonusType":bonusType,
        "bonusDice":bonusDice,
        "bonusMax":max(bonusDice) if bonusDice else 0,
        "statMod":statModifier,
        "rollSum":rollSum
    }

    return results

def rollWoD( expression, rollType ):

    if rollType == "exalted" or rollType == "exvswod":
        defaultTarget = 7
    else:
        defaultTarget = 8

    rollTerms = expression.split()

    if len(rollTerms) == 0:
        raise BadExpressionError
    elif len(rollTerms) == 1:
        target = defaultTarget
    elif len(rollTerms) >= 2:
        target = "7" if not re.match("[0-9]+",rollTerms[1]) else rollTerms[1]

    if re.match( "^[0-9]+$", rollTerms[0] ):
        hitRoll = rollHits( str(rollTerms[0]) + "d10 " + str(target) )
    elif re.match( "^[0-9]+d10$", rollTerms[0] ):
        hitRoll = rollHits( str(rollTerms[0]) + " " + str(target) )
    else:
        raise BadExpressionError
    
    if len(rollTerms) >=3:
        mods = rollTerms[2]
    elif len(rollTerms) == 2 and re.match("[SsCc]+",rollTerms[1]):
        mods = rollTerms[1]
    else:
        mods = ""

    hitRoll["isBotch"] = False

    if hitRoll["numHits"] == 0:
        for d in hitRoll["dice"]:
            if d == 1:
                hitRoll["isBotch"] = True
                break
        
    if ( rollType == "exvswod" and re.search("[Ss]",mods) ) or rollType == "exalted":
        for d in hitRoll["dice"]:
            if d == 10:
                hitRoll["numHits"] += 1 

    if rollType == "exvswod" and hitRoll["numHits"] > 0 and not re.search("[Cc]",mods):
        for d in hitRoll["dice"]:
            if d == 1:
                hitRoll["numHits"] -= 1

    if hitRoll["numHits"] < 0:
        hitRoll["numHits"] = 0

    return hitRoll

if __name__ == "__main__":
    import json
    testExpr = "1"
    print("Rolling " + testExpr)
    try:
        print(rollWoD(testExpr))
    except (ValueError, BadExpressionError):
        print("error")
    # results = rollLancer(testExpr)
    # import json
    # print(json.dumps(results, indent=4))
    # print("\n\n")
    # import utils
    # print("You have rolled 1d20 " + ( "+ " + results['expression'] if results['expression'] else "with no bonuses" )
    #       + ": **" + str(results['rollSum']) + "**"
    #       + "\n<" + str(results['baseRoll']) + "> + "
    #       + "stat mod " + str(results['statMod']) + " + "
    #       + "bonus dice " + utils.list2string(results['bonusDice'])
    #       + " => " + str(results['rollSum']))
