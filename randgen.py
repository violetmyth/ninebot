from random import randint

def generateComboSummon(filename):
    callListFile = open(filename)
    summonCalls=[]
    for line in callListFile:
        summonCalls.append( line.rstrip() )
    callListFile.close()

    firstLineNumber = randint( 0, len(summonCalls) - 1 )
    secondLineNumber = randint( 0, len(summonCalls) - 1 )

    firstSummon = summonCalls[firstLineNumber].split(",")
    secondSummon = summonCalls[secondLineNumber].split(",")

    result = {
        "combination":firstSummon[0] + " + " + secondSummon[0],
        "comboCall":firstSummon[1] + " " + secondSummon[2]
    }

    return result

if __name__ == "__main__":
    testResults = generateComboSummon("gen_combosummon.txt")
    print( testResults["combination"] + ":\n" + testResults["comboCall"] )