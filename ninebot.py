# pip imports
import discord
import json
from random import choice
import os
import re
from sys import exit


# local files
import dice
import utils
import randgen

class NoTargetError(Exception):
    pass

class BadExpressionError(Exception):
    pass

print("Ninebot is starting...")

configFile = open('ninebot.conf','r')
botConfig = json.loads( configFile.read() )

serverSettings = botConfig["servers"]
token = botConfig["token"]

configFile.close()

intents = discord.Intents(guild_messages=True,guilds=True,message_content=True)

client = discord.Client(intents=intents)

@client.event
async def on_ready():
    print('Authenticated as {0.user}'.format(client))

@client.event
async def on_message(message):
    serverId = str(message.guild.id)
    if serverId not in serverSettings.keys():
        return

    msgChannel = client.get_channel(message.channel.id).name
    try:
        signal = serverSettings[serverId]["signalCharacter"]
    except:
        return

    if ( message.author == client.user ):
        return

    if serverSettings[serverId]["channelListType"] == "allow":
        if msgChannel in serverSettings[serverId]["channels"]:
            processMessage = True
    elif serverSettings[serverId]["channelListType"] == "block":
        if msgChannel not in serverSettings[serverId]["channels"]:
            processMessage = True

    if not processMessage:
        return
    elif message.content == signal + 'spec.gif':
        await message.channel.send('HAHA TIME FOR TANKS https://i.imgur.com/HMhABcJ.gif')
    elif message.content == signal + 'omy.png':
        await message.channel.send('zehi mo nashi! https://i.imgur.com/aqpTxqu.png')
    elif message.content == signal + 'vi.jpg' or message.content == signal + 'vi.jpeg':
        await message.channel.send('poor disaster lesbian... https://i.imgur.com/eEAhYSL.jpeg')
    elif message.content == signal + 'chart':
        await message.channel.send('muh immershun http://unionrp.co.uk/chart.' + choice(["png","jpg","gif"]))
    elif message.content == signal + 'oyoyo':
        await message.channel.send('OYOYO! https://imgur.com/a/QP8tRE7')

    elif message.content.startswith( signal + "roll " ):
        msgNoCommand = message.content.replace(signal + "roll ","")
        try:
            results = dice.rollDice(msgNoCommand)
            await message.channel.send( message.author.mention 
                                        + " rolled "
                                        + results['expression']
                                        + ": "
                                        + str(results['sum'])
                                        + "\n"
                                        + utils.list2string(results['dice']) )
        except ValueError:
            await message.channel.send(message.author.mention + ", I couldn't understand your die roll." )

    elif message.content.startswith( signal + "rollhits " ):
        msgNoCommand = message.content.replace(signal + "rollhits ","")
        try:
            results = dice.rollHits(msgNoCommand)
            await message.channel.send( message.author.mention 
                                        + " rolled "
                                        + results['expression']
                                        + " (target number "
                                        + str(results['targetNumber'])
                                        + "): "
                                        + str(results['numHits'])
                                        + " hits\n"
                                        + utils.list2string(results['dice']) )
        except ValueError:
            await message.channel.send(message.author.mention + ", I couldn't understand your die roll." )
        except NoTargetError:
            await message.channel.send(message.author.mention + ", your roll seems to be missing a target number." )

    elif re.match("\\" + signal + "lr($|\ +)", message.content):
        msgNoCommand = message.content.replace(signal + "lr","")
        try:
            results = dice.rollLancer(msgNoCommand)

            if len(results['bonusDice']) > 0:
                bonusString = utils.list2string(results['bonusDice'])
                for i in range(0,6):
                    if i + 1 != results['bonusMax']:
                        bonusString = bonusString.replace(str(i + 1), "~~" + str(i+1) + "~~")
            else:
                bonusString = "[]"


            await message.channel.send(message.author.mention
                                       + " rolled 1d20 " + ( "+ " + results['expression'] if results['expression'] else "with no bonuses" )
                                       + ": **" + str(results['rollSum']) + "**"
                                       + "\n<" + str(results['baseRoll']) + "> + "
                                       + "stat mod " + str(results['statMod'])
                                       + (" - difficulty " if results['bonusType'] == -1 else " + accuracy ") + bonusString
                                       + " => " + str(results['rollSum']) )

        except ValueError:
            await message.channel.send(message.author.mention + ", I couldn't understand your die roll." )

    elif re.match("\\" + signal + "(wod|ex|exalted|exvswod)($|\ +)", message.content):
        if re.match("\\" + signal + "wod($|\ +)", message.content):
            msgNoCommand = message.content.replace(signal + "wod","")
            rollType = "wod"
        elif re.match("\\" + signal + "ex($|\ +)", message.content):
            msgNoCommand = message.content.replace(signal + "ex","")
            rollType = "exalted"
        elif re.match("\\" + signal + "exalted($|\ +)", message.content):
            msgNoCommand = message.content.replace(signal + "exalted","")
            rollType = "exalted"
        elif re.match("\\" + signal + "exvswod($|\ +)", message.content):
            msgNoCommand = message.content.replace(signal + "exvswod","")
            rollType = "exvswod"

        try:
            results = dice.rollWoD(msgNoCommand, rollType)

            await message.channel.send( message.author.mention 
                                        + " rolled "
                                        + results['expression']
                                        + " (target "
                                        + str(results['targetNumber'])
                                        + "): "
                                        + str(results['numHits'])
                                        + " hits\n"
                                        + utils.list2string(results['dice'])
                                        + ( "\n\nBotch!" if results['isBotch'] else ""))

        except (ValueError, BadExpressionError):
            await message.channel.send(message.author.mention + ", I couldn't understand your die roll." )
        except NoTargetError:
            await message.channel.send(message.author.mention + ", your roll seems to be missing a target number." )
    
    elif message.content == signal + "combosummon":
        comboSummon = randgen.generateComboSummon("gen_combosummon.txt")
        await message.channel.send(comboSummon["combination"] + ":\n" + comboSummon["comboCall"])
    
    else:
        pass
        #print("No bot command found")

client.run(token)
